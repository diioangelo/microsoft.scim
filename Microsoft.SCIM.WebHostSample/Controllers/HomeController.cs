﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace Microsoft.SCIM.WebHostSample.Controllers
{
    public class HomeController : Controller
    {
        public HomeController() : base()
        {
        }

        public IActionResult Index() => Ok("Up and running!");
    }
}

