FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /source
COPY . /source

WORKDIR /source/Microsoft.SystemForCrossDomainIdentityManagement
RUN dotnet restore

WORKDIR /source/Microsoft.SCIM.WebHostSample
RUN dotnet publish -c Debug -o ../../app

ENV ASPNETCORE_URLS http://+:27976
EXPOSE 27976/tcp

# Build runtime image
WORKDIR /app
ENTRYPOINT ["dotnet", "Microsoft.SCIM.WebHostSample.dll"]
